CREATE TABLE IF NOT EXISTS users (
	id VARCHAR(36) PRIMARY KEY,
	fullname VARCHAR(40) NOT NULL,
	email VARCHAR(64) NOT NULL,
	password VARCHAR(20) NOT NULL,
	UNIQUE KEY UK_email (email)
);

CREATE TABLE IF NOT EXISTS lotteries (
	id INT AUTO_INCREMENT PRIMARY KEY,
	savingdate DATE,
	email VARCHAR(64) NOT NULL,
	firstnumber INT NOT NULL,
	secondnumber INT NOT NULL,
	thirdnumber INT NOT NULL,
	fourthnumber INT NOT NULL,
	fifthnumber INT NOT NULL
);