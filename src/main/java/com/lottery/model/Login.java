package com.lottery.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

public class Login {

	@NotBlank
	@Email
	private String emailAddress; //unique key
	
	@NotBlank
	@Size(min = 6, max = 20)
	private String password;

	public Login() {}
	
	public Login(@NotBlank @Email String emailAddress, @NotBlank @Size(min = 6, max = 20) String password) {
		this.emailAddress = emailAddress;
		this.password = password;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Login [emailAddress=" + emailAddress + ", password=" + password + "]";
	}

}
