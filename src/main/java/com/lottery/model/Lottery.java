package com.lottery.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.constraints.Digits;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class Lottery {

	@NotNull
	@Min(1)
	@Max(90)
	private int firstNumber;
	
	@NotNull
	@Min(1)
	@Max(90)
	@Digits(integer=2, fraction=0)
	private int secondNumber;
	
	@NotNull
	@Min(1)
	@Max(90)
	@Digits(integer=2, fraction=0)
	private int thirdNumber;
	
	@NotNull
	@Min(1)
	@Max(90)
	@Digits(integer=2, fraction=0)
	private int fourthNumber;
	
	@NotNull
	@Min(1)
	@Max(90)
	@Digits(integer=2, fraction=0)
	private int fifthNumber;
	
	public Lottery() {}

	public Lottery(@NotBlank @Size(min = 1, max = 90) int firstNumber,
			@NotBlank @Size(min = 1, max = 90) int secondNumber, @NotBlank @Size(min = 1, max = 90) int thirdNumber,
			@NotBlank @Size(min = 1, max = 90) int fourthNumber, @NotBlank @Size(min = 1, max = 90) int fifthNumber) {
		this.firstNumber = firstNumber;
		this.secondNumber = secondNumber;
		this.thirdNumber = thirdNumber;
		this.fourthNumber = fourthNumber;
		this.fifthNumber = fifthNumber;
	}

	public int getFirstNumber() {
		return firstNumber;
	}

	public void setFirstNumber(int firstNumber) {
		this.firstNumber = firstNumber;
	}

	public int getSecondNumber() {
		return secondNumber;
	}

	public void setSecondNumber(int secondNumber) {
		this.secondNumber = secondNumber;
	}

	public int getThirdNumber() {
		return thirdNumber;
	}

	public void setThirdNumber(int thirdNumber) {
		this.thirdNumber = thirdNumber;
	}

	public int getFourthNumber() {
		return fourthNumber;
	}

	public void setFourthNumber(int fourthNumber) {
		this.fourthNumber = fourthNumber;
	}

	public int getFifthNumber() {
		return fifthNumber;
	}

	public void setFifthNumber(int fifthNumber) {
		this.fifthNumber = fifthNumber;
	}	

	@Override
	public String toString() {
		return "Lottery [firstNumber=" + firstNumber + ", secondNumber=" + secondNumber + ", thirdNumber=" + thirdNumber
				+ ", fourthNumber=" + fourthNumber + ", fifthNumber=" + fifthNumber + "]";
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + fifthNumber;
		result = prime * result + firstNumber;
		result = prime * result + fourthNumber;
		result = prime * result + secondNumber;
		result = prime * result + thirdNumber;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Lottery other = (Lottery) obj;
		if (fifthNumber != other.fifthNumber)
			return false;
		if (firstNumber != other.firstNumber)
			return false;
		if (fourthNumber != other.fourthNumber)
			return false;
		if (secondNumber != other.secondNumber)
			return false;
		if (thirdNumber != other.thirdNumber)
			return false;
		return true;
	}
	
	/**
	 * Creates a List from the fields, so we can iterate over it.
	 * @return
	 */
	public List<Integer> fieldsToList() {
		List<Integer> result = new ArrayList<Integer>();
		
		result.add(this.firstNumber);
		result.add(this.secondNumber);
		result.add(this.thirdNumber);
		result.add(this.fourthNumber);
		result.add(this.fifthNumber);
		
		return result;
	}
	
	/**
	 * The Key in the Map is the index of the lottery number
	 * and the Value will be true if there is the same number
	 * in the other Lottery object (can be in an other position too)
	 * e. g.
	 * 1.
	 * 	this.firstNumber = 2
	 * 	other.firstNumber = 2
	 * 
	 * 	this.getFirstNumber() == other.getFirstNumber()
	 * 
	 * 2.
	 * 	this.firstNumber = 2
	 * 	other.fourthNumber = 2
	 * 
	 * 	this.getFirstNumber() != other.getFirstNumber()
	 *  this.getFirstNumber() == other.getFourthNumber()
	 *  
	 * @param other - the other Lottery object
	 * @return A Map with an Integer key and a Boolean value, where the key represents the position of the number,
	 * and the value represents if there a matching number in the other Lottery object.
	 */
	public Map<Integer, Boolean> compare(List<Integer> other) {
		Map<Integer, Boolean> result = new HashMap<>();
		List<Integer> t = this.fieldsToList();
		
		for (int i = 0; i < other.size(); i++) {
			if (searchForMatchingNumber(t.get(i), other)) {
				result.put(i, true);
			} else {
				result.put(i, false);
			}
		}
		
		return result;
	}
	
	/**
	 * 
	 * @param number - the current lottery number
	 * @param other - the other lottery object's numbers
	 * @return true if there's a same number like 'number' in 'other'; false otherwise.
	 */
	private boolean searchForMatchingNumber(int number, List<Integer> other) {
		
		for (int n : other) {
			if (n == number) {
				return true;
			}
		}
		
		return false;
	}
	
}
