package com.lottery.model;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class User {

	@NotNull
	private int id;
	
	@NotBlank
	@Size(min = 6, max = 40)
	private String fullName;
	
	@NotBlank
	@Email
	private String emailAddress; //unique key
	
	@NotBlank
	@Size(min = 6, max = 20)
	private String password;
	
	public User() {}

	public User(@NotBlank @Size(min = 6, max = 40) String fullName, @NotBlank @Email String emailAddress,
			@NotBlank @Size(min = 6, max = 20) String password) {
		this.fullName = fullName;
		this.emailAddress = emailAddress;
		this.password = password;
	}

	public String getFullName() {
		return fullName;
	}

	public void setFullName(String fullName) {
		this.fullName = fullName;
	}

	public String getEmailAddress() {
		return emailAddress;
	}

	public void setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "User [fullName=" + fullName + ", emailAddress=" + emailAddress + ", password=" + password + "]";
	}

}
