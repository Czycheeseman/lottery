package com.lottery.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.lottery.model.User;
import com.lottery.service.RegisterService;
import com.lottery.service.exception.UserAlreadyExistsException;

@Controller
public class RegisterController {

	private static final String REGISTER_FORM_ATTRIBUTE = "registerForm";
	
	private static final String PATH_REGISTER = "register-form.html";
	private static final String PATH_REGISTER_FORM_SUBMIT = "/register";
	
	private static final String PAGE_REGISTER = "register-form";
	private static final String PAGE_REDIRECT_HOME = "redirect:/";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private RegisterService registerService;
	
	@GetMapping(PATH_REGISTER)
	public String showRegisterForm(@ModelAttribute(REGISTER_FORM_ATTRIBUTE) User user) {
		logger.info("Getting to the Register page!");
		return PAGE_REGISTER;
	}
	
	@PostMapping(PATH_REGISTER_FORM_SUBMIT)
	public String sendRegister(@ModelAttribute(REGISTER_FORM_ATTRIBUTE) @Valid User user, BindingResult result,
			HttpServletResponse response) throws UserAlreadyExistsException {

		String target = PAGE_REGISTER;
		
		if (result.hasErrors()) {
			logger.error("An error has occurred at the Register form!");
			
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.reject("form.error.incomplete.input");	
		} else {	
			try {
				logger.info("Sending the following User from the Register page: " + user);
				
				registerService.register(user);
				
				target = PAGE_REDIRECT_HOME;
			} catch(UserAlreadyExistsException e) {
				logger.error("A UserAlreadyExistsException has been caught in the Register Controller!");
				
				result.reject("error.user.already.exists");
			}			
		}

		return target;
	}
}
