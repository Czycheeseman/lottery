package com.lottery.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;

import com.lottery.service.LotteryService;

@Controller
public class IndexPageController {

	private static final String SESSION_ATTRIBUTE_LOGGED_IN_USER = "loggedInUser";
	
	//Page
	private static final String PAGE_INDEX = "index";
	private static final String PAGE_LOTTERY = "lottery-form";
	private static final String PAGE_REGISTER = "register-form";
	private static final String PAGE_LOGIN = "login-form";
	
	//Path
	private static final String PATH_INDEX = "/";
	private static final String PATH_LOTTERY = "/lottery-form.html";
	private static final String PATH_REGISTER = "/register-form.html";
	private static final String PATH_LOGIN = "/login-form.html";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private LotteryService lotteryService;
	
	/**
	 *  This here sets the main page of the web-application.
	 * 
	 * This handles all requests coming to "/" and returns the view called 'index' (index.html)
	 * 
	 * Shortcut to @RequestMapping(method = RequestMethod.GET)
	 * @GetMapping handles the HTTP GET requests matched with the
	 * given URI
	 */
	@GetMapping(PATH_INDEX)
	public String getIndexPage(Model model, HttpSession session) {
		
		model.addAttribute("recentLotteryNumbers", lotteryService.getLotteryNumbersByGivenRowAmountExceptLastWeek(5));
		
		if (session.getAttribute(SESSION_ATTRIBUTE_LOGGED_IN_USER) != null) {
			logger.info("The User is set in the session!");
			model.addAttribute(SESSION_ATTRIBUTE_LOGGED_IN_USER, session.getAttribute(SESSION_ATTRIBUTE_LOGGED_IN_USER));	
		}
		
		logger.info("Getting to the Index/Home page!");
		return PAGE_INDEX;
	}
	
	/**
	 *This takes the user to the Login page.
	 * 
	 * Sends a POST request to /login-form.html which will be caught by the LoginController.
	 */
	@PostMapping(PATH_LOGIN)
	public String goToLoginPage() {
		logger.info("Posting to the Login page!");
		return PAGE_LOGIN;
	}
	
	/**
	 * This takes the user to the Register page.
	 * 
	 */
	@PostMapping(PATH_REGISTER)
	public String goToRegisterPage() {
		logger.info("Posting to the Register page!");
		return PAGE_REGISTER;
	}
	
	/**
	 * This takes the user to the Lottery page.
	 * 
	 */
	@PostMapping(PATH_LOTTERY)
	public String goToLotteryPage() {
		logger.info("Posting to the Lottery page!");
		return PAGE_LOTTERY;
	}

}
