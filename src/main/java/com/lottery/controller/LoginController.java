package com.lottery.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.lottery.model.Login;
import com.lottery.model.User;
import com.lottery.service.LoginService;
import com.lottery.service.exception.UserDoesNotExistException;

@Controller
public class LoginController {

	private static final String LOGIN_FORM_ATTRIBUTE = "loginForm";
	
	private static final String PATH_LOGIN = "login-form.html";
	private static final String PATH_LOGIN_FORM_SUBMIT = "/login";
	
	private static final String PAGE_LOGIN = "login-form";
	private static final String PAGE_REDIRECT_HOME = "redirect:/";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private LoginService loginService;
	
	/*The IndexPageController sends a POST request, with the
	 * value of /login-form.html, which will be caught here
	 * */
	@GetMapping(PATH_LOGIN)
	public String showLoginForm(@ModelAttribute(LOGIN_FORM_ATTRIBUTE) Login login) {
		logger.info("Getting to the Login page!");
		return PAGE_LOGIN;
	}
	
	@PostMapping(PATH_LOGIN_FORM_SUBMIT)
	public String sendLogin(@ModelAttribute(LOGIN_FORM_ATTRIBUTE) @Valid Login login, BindingResult result,
			HttpServletResponse response, HttpSession session) {

		String target = PAGE_LOGIN;
		User u = null;
		
		if (result.hasErrors()) {
			logger.error("An error has occurred at the Login form!");
			
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.reject("form.error.incomplete.input");
		} else {
			
			logger.info("Sending the following User (login) from the Login page: " + login);
			
			try {	
				u = loginService.validateUser(login);		
			} catch (UserDoesNotExistException e) {
				logger.error("UserDoesNotExistException caught in the LoginController!");
				result.reject("error.user.does.not.exist");
			}
			
			if (u != null) {
				logger.info("Setting the User in the session with name: 'loggedInUser' !");
				session.setAttribute("loggedInUser", u);
				target = PAGE_REDIRECT_HOME;
			}	

		}

		return target;
	}
	
}
