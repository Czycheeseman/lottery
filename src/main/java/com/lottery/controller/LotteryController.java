package com.lottery.controller;

import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import com.lottery.model.Lottery;
import com.lottery.model.User;
import com.lottery.service.LotteryService;
import com.lottery.validator.LotteryFormValidator;

@Controller
public class LotteryController {

	private static final String SESSION_ATTRIBUTE_LOGGED_IN_USER = "loggedInUser";
	
	private static final String PATH_LOTTERY = "lottery-form.html";
	private static final String PATH_LOTTERY_FORM_SUBMIT = "/lottery";
	
	private static final String PAGE_LOTTERY = "lottery-form";
	private static final String PAGE_RESULT_REDIRECT = "redirect:/result";
	
	private static final String LOTTERY_FORM_ATTRIBUTE = "lotteryForm";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private LotteryService lotteryService;
	
	@Autowired
	private LotteryFormValidator formValidator;
	
	@InitBinder(LOTTERY_FORM_ATTRIBUTE)
	public void initBinder(WebDataBinder binder) {
		binder.addValidators(formValidator);
	}
	
	@GetMapping(PATH_LOTTERY)
	public String showLoginForm(@ModelAttribute(LOTTERY_FORM_ATTRIBUTE) Lottery lottery) {
		logger.info("Getting to the Lottery page!");
		return PAGE_LOTTERY;
	}
	
	@PostMapping(PATH_LOTTERY_FORM_SUBMIT)
	public String sendInputNumbers(@ModelAttribute(LOTTERY_FORM_ATTRIBUTE) @Valid Lottery lottery, BindingResult result,
			HttpSession session, HttpServletResponse response) {
		
		String target = PAGE_LOTTERY;
		
		if (result.hasErrors()) {
			logger.error("An error occurred at the Lottery form!");
			
			response.setStatus(HttpStatus.BAD_REQUEST.value());
			result.reject("form.error.incomplete.input");
			
		} else {
			logger.info("Sending the following Lottery numbers: " + lottery);
			
			logger.info("Setting the session attribute 'enteredLotteryNumbers'!");
			session.setAttribute("enteredLotteryNumbers", lottery);
			
			logger.info("Setting the session attribute 'result'!");
			session.setAttribute("result", lotteryService.checkIfTheUserHaveWon(lottery));
			
			logger.info("Setting the session attribute 'thisWeeksNumbers'!");
			session.setAttribute("thisWeeksNumbers", lotteryService.getLotteryNumbersByGivenRowAmount(1));
			
			if (session.getAttribute(SESSION_ATTRIBUTE_LOGGED_IN_USER) != null) {
				lotteryService.saveLotteryNumbers((User) session.getAttribute(SESSION_ATTRIBUTE_LOGGED_IN_USER), lottery);
			}
			
			target = PAGE_RESULT_REDIRECT;
		}

		return target;
	}
}
