package com.lottery.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ResultController {
	
	private static final String PATH_RESULT = "/result";
	
	private static final String PAGE_RESULT = "result-page";
	
	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping(PATH_RESULT)
	public String getResult(Model model, HttpSession session) {
		
		logger.info("Displaying the result!");
		model.addAttribute("haveTheUserWon", session.getAttribute("result"));
		
		logger.info("Displaying the entered lottery numbers!");
		model.addAttribute("enteredLotteryNumbers", session.getAttribute("enteredLotteryNumbers"));
		
		logger.info("Displaying this week's lottery numbers!");
		model.addAttribute("thisWeeksNumbers", session.getAttribute("thisWeeksNumbers"));
		return PAGE_RESULT;
	}
}
