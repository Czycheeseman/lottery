package com.lottery.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class LogoutController {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@GetMapping("/logout.html")
	public String logout(HttpSession session) {
		
		logger.info("Logging out...");
		logger.info("Setting the session attribute named 'loggedInUser' value to 'null' !");
		session.setAttribute("loggedInUser", null);
		
		return "redirect:/";
	}
	
}
