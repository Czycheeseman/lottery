package com.lottery.validator;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.lottery.model.Lottery;

@Component
public class LotteryFormValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return Lottery.class.isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		Lottery lottery = (Lottery) target;
		
		List<Integer> numbers = lottery.fieldsToList();
		Set<Integer> set = new HashSet<Integer> (numbers);
		
		//If the set's size is smaller, than the list's, then there are duplicates in the list
		if (set.size() < numbers.size()) {
			errors.rejectValue(null, "lottery.form.error.matching.input.numbers");
		}
	}

}
