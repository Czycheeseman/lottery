package com.lottery.service.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lottery.model.User;
import com.lottery.repository.RegisterRepository;
import com.lottery.service.RegisterService;
import com.lottery.service.exception.UserAlreadyExistsException;

@Service
public class RegisterServiceImpl implements RegisterService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private RegisterRepository registerRepository;
	
	@Override
	@Transactional(rollbackFor = UserAlreadyExistsException.class)
	public void register(@NotNull @Valid User user) throws UserAlreadyExistsException {
		
		logger.info("Registering user: " + user);
		
		try {
			registerRepository.save(user);
		} catch (DuplicateKeyException duplicate) {
			logger.error("Error during persisting the user", duplicate);
			throw new UserAlreadyExistsException("User already exists with email: " + user.getEmailAddress());
		}
	}

}
