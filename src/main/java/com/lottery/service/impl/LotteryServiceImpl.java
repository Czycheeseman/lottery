package com.lottery.service.impl;

import java.util.List;
import java.util.Map;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lottery.model.Lottery;
import com.lottery.model.User;
import com.lottery.repository.LotteryRepository;
import com.lottery.service.LotteryService;

@Service
public class LotteryServiceImpl implements LotteryService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private LotteryRepository lotteryRepository;
	
	@Override
	public @NotNull List<Lottery> getLotteryNumbersByGivenRowAmount(@NotNull @Min(1) int numberOfRows) {
		logger.info("Getting the lottery numbers from the past " + numberOfRows + " weeks!");
		return lotteryRepository.getLotteryNumbersByGivenRowAmount(numberOfRows);
	}

	@Override
	public @NotNull List<Lottery> getLotteryNumbersByGivenRowAmountExceptLastWeek(@NotNull @Min(1) int numberOfRows) {
		logger.info("Getting the lottery numbers from the past " + numberOfRows+1 + " weeks, except the previous week!");
		return lotteryRepository.getLotteryNumbersByGivenRowAmountExceptFirstRow(numberOfRows);
	}

	@Override
	public boolean checkIfTheUserHaveWon(@NotNull @Valid Lottery enteredNumbers) {
		logger.info("Checking if the user have won!");
		
		Lottery previousWeeksNumbers = getLotteryNumbersByGivenRowAmount(1).get(0);
		Map<Integer, Boolean> map = previousWeeksNumbers.compare(enteredNumbers.fieldsToList());
		boolean result = false;
		
		int trueCount = 0;
		for (Map.Entry<Integer, Boolean> entry : map.entrySet()) {
			if (entry.getValue() == true) {
				trueCount++;
			}
		}
		
		if (previousWeeksNumbers.equals(enteredNumbers) || trueCount == 5) {
			result = true;
		}
		
		return result;
	}

	@Override
	public void saveLotteryNumbers(@NotNull @Valid User user, @NotNull @Valid Lottery lottery) {
		logger.info("Saving the lottery numbers into the 'lotteries' table, using the following object: " + user + lottery);
		lotteryRepository.saveLotteryNumbers(user, lottery);
	}
}
