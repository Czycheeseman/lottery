package com.lottery.service.impl;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lottery.model.Login;
import com.lottery.model.User;
import com.lottery.repository.LoginRepository;
import com.lottery.service.LoginService;
import com.lottery.service.exception.UserDoesNotExistException;

@Service
public class LoginServiceImpl implements LoginService {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private LoginRepository loginRepository;
	
	@Override
	public User validateUser(@NotNull @Valid Login login) throws UserDoesNotExistException {
		
		logger.info("Validating the following User (login): " + login);
		
		User u = null;
		
		try {
			 u = loginRepository.validateUser(login);
		} catch (UserDoesNotExistException e) {
			throw new UserDoesNotExistException("User with e-mail " + login.getEmailAddress() + " does not exist!");
		}
		
		logger.info("Successfully validated the User (login): " + login);

		return u;
	}

}
