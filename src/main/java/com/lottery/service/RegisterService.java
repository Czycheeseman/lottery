package com.lottery.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.lottery.model.User;
import com.lottery.service.exception.UserAlreadyExistsException;

public interface RegisterService {

	void register(@NotNull @Valid User user) throws UserAlreadyExistsException;
	
}
