package com.lottery.service;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.lottery.model.Lottery;
import com.lottery.model.User;

public interface LotteryService {

	@NotNull List<Lottery> getLotteryNumbersByGivenRowAmount(@NotNull @Min(1) int numberOfRows);
	
	@NotNull List<Lottery> getLotteryNumbersByGivenRowAmountExceptLastWeek(@NotNull @Min(1) int numberOfRows);
	
	boolean checkIfTheUserHaveWon(@NotNull @Valid Lottery enteredNumbers);
	
	void saveLotteryNumbers(@NotNull @Valid User user, @NotNull @Valid Lottery lottery);
}
