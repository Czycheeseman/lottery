package com.lottery.service;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.lottery.model.Login;
import com.lottery.model.User;
import com.lottery.service.exception.UserDoesNotExistException;

public interface LoginService {

	User validateUser(@NotNull @Valid Login login) throws UserDoesNotExistException;
	
}
