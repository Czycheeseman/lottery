package com.lottery.repository.impl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.security.cert.X509Certificate;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.net.ssl.*;
import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.lottery.model.Lottery;
import com.lottery.model.User;
import com.lottery.repository.LotteryRepository;

@Repository
public class LotteryRepositoryImpl implements LotteryRepository {

	private static final String OTOS_LOTTERY_URL = "https://bet.szerencsejatek.hu/cmsfiles/otos.csv";
	
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void saveLotteryNumbers(@NotNull @Valid User user, @NotNull @Valid Lottery lottery) {
		
		String sql = "INSERT INTO lotteries (savingdate, email, firstnumber, secondnumber, thirdnumber, fourthnumber, fifthnumber)"
				+ "VALUES (?, ?, ?, ?, ?, ?, ?)";
		
		logger.info("Inserting into the 'lotteries' table! The following objects are used: " + user + lottery);
		
		jdbcTemplate.update(sql, new Date(System.currentTimeMillis()), user.getEmailAddress(), lottery.getFirstNumber(),
				lottery.getSecondNumber(), lottery.getThirdNumber(), lottery.getFourthNumber(), lottery.getFifthNumber());
		
	}
	
	/**
	 * Returns a List containing the five lottery numbers for each week.
	 * The number of weeks depends on the parameter called: @param(numberOfRows)
	 */
	
	@Override
	public @NotNull List<Lottery> getLotteryNumbersByGivenRowAmount(@NotNull @Min(1) int numberOfRows) {
		logger.info("Getting the lottery numbers from the past " + numberOfRows + " weeks!");
		return getLotteryNumbersFromCSVFileFromURL(OTOS_LOTTERY_URL, numberOfRows, true);
	}
	
	@Override
	public @NotNull List<Lottery> getLotteryNumbersByGivenRowAmountExceptFirstRow(@NotNull @Min(1) int numberOfRows) {
		logger.info("Getting the lottery numbers from the past " + numberOfRows+1 + " weeks, except the previous week!");
		return getLotteryNumbersFromCSVFileFromURL(OTOS_LOTTERY_URL, numberOfRows, false);
	}
	
	 private List<Lottery> getLotteryNumbersFromCSVFileFromURL(String goalURL, int numberOfRows, boolean withFirstRow) {
	     
		 List<Lottery> numbers = new ArrayList<Lottery>();
		 
		 try {
	            disableSSLValidation();

	            URL url = new URL(goalURL);
	            URLConnection conn = url.openConnection();
	            String line = "";
	            String csvSplitBy = ";";
	            String[] lottery;

	            try (BufferedReader reader = new BufferedReader(new InputStreamReader(conn.getInputStream()))) {

	            	int i = 1;
	            	
	            	if (withFirstRow) {
	            		
		                while (i <= numberOfRows) {
		                	
		                	line = reader.readLine();

			                lottery = line.split(csvSplitBy);

			                numbers.add(new Lottery(Integer.parseInt(lottery[lottery.length - 5]), Integer.parseInt(lottery[lottery.length - 4]),
			                		Integer.parseInt(lottery[lottery.length - 3]), Integer.parseInt(lottery[lottery.length - 2]),
			                		Integer.parseInt(lottery[lottery.length - 1])));
			                
			                i++;
		                }
	            	} else {
	            		
		                while (i <= numberOfRows + 1) {
		                	
		                	line = reader.readLine();
	                		
			                lottery = line.split(csvSplitBy);
		                	
		                	if (i != 1) {
				                numbers.add(new Lottery(Integer.parseInt(lottery[lottery.length - 5]), Integer.parseInt(lottery[lottery.length - 4]),
				                		Integer.parseInt(lottery[lottery.length - 3]), Integer.parseInt(lottery[lottery.length - 2]),
				                		Integer.parseInt(lottery[lottery.length - 1])));
		                	}
			                
			                i++;
		                }
	            	}                
	            }
	        } catch (IOException e) {
	            e.printStackTrace();
	            logger.error("I/O error at the URL!");
	        }
	        
	        return numbers;
	 }
	 
	//Have to disable the SSL validation unless I cannot access the .csv file from the page of Szerencsejatek
	private Boolean disableSSLValidation() {

        try {
            final SSLContext sslContext = SSLContext.getInstance("TLS");

            sslContext.init(null, new TrustManager[]{new X509TrustManager() {

                public void checkClientTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                }

                public void checkServerTrusted(X509Certificate[] x509Certificates, String s) throws CertificateException {
                }

                public X509Certificate[] getAcceptedIssuers() {
                    return new X509Certificate[0];
                }
            }}, null);

            HttpsURLConnection.setDefaultSSLSocketFactory(sslContext.getSocketFactory());
            HttpsURLConnection.setDefaultHostnameVerifier(new HostnameVerifier() {
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });

        } catch (KeyManagementException e) {
            e.printStackTrace();
            logger.error("KeyManagementException thrown while disabling the SSL validation!");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
            logger.error("NoSuchAlgorithmException thrown while disabling the SSL validation!");
        }

        return true;
    }
}
