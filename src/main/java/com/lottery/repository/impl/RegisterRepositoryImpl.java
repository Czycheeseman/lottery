package com.lottery.repository.impl;

import java.util.UUID;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.jdbc.JdbcTemplateAutoConfiguration;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.lottery.model.User;
import com.lottery.repository.RegisterRepository;

@Repository
public class RegisterRepositoryImpl implements RegisterRepository {

	private final Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	@Override
	public void save(@NotNull @Valid User user) {
		logger.info("Inserting the following user into the 'users' table: " + user);
		
		final String sql = "INSERT INTO users (id, fullname, email, password) VALUES (?, ?, ?, ?)";
		final String id = UUID.randomUUID().toString();
		
		jdbcTemplate.update(sql, id, user.getFullName(), user.getEmailAddress(), user.getPassword());
	}

}
