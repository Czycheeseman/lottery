package com.lottery.repository.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

import com.lottery.model.Login;
import com.lottery.model.User;
import com.lottery.repository.LoginRepository;
import com.lottery.service.exception.UserDoesNotExistException;

@Repository
public class LoginRepositoryImpl implements LoginRepository {

	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	private final RowMapper<User> mapper = new RowMapper<User>() {

		@Override
		public User mapRow(ResultSet rs, int rowNum) throws SQLException {
			
			User u = new User();

			u.setFullName(rs.getString("fullname"));
			u.setEmailAddress(rs.getString("email"));
			u.setPassword(rs.getString("password"));
			
			return u;
		}
		
	};
	
	@Override
	public User validateUser(@NotNull @Valid Login login) throws UserDoesNotExistException {
		
		logger.info("LoginRepository: validating the User (login): " + login);
		
		String sql = "SELECT * FROM users WHERE email = '" + login.getEmailAddress() + "'" + "AND password = '" + login.getPassword() + "'";
		
		List<User> user = jdbcTemplate.query(sql, mapper);

		if (user.size() == 0) {
			throw new UserDoesNotExistException("User with e-mail " + login.getEmailAddress() + " does not exist!");
		}
		
		logger.info("LoginRepository: successfully validated the User (login): " + user.get(0));
		
		return user.get(0);
	}

}
