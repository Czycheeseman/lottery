package com.lottery.repository;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

import com.lottery.model.User;

public interface RegisterRepository {

	void save(@NotNull @Valid User user);
	
}
