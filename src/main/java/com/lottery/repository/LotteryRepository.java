package com.lottery.repository;

import java.util.List;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import com.lottery.model.Lottery;
import com.lottery.model.User;

public interface LotteryRepository {

	@NotNull List<Lottery> getLotteryNumbersByGivenRowAmount(@NotNull @Min(1) int numberOfRows);
	
	@NotNull List<Lottery> getLotteryNumbersByGivenRowAmountExceptFirstRow(@NotNull @Min(1) int numberOfRows);
	
	void saveLotteryNumbers(@NotNull @Valid User user, @NotNull @Valid Lottery lottery);
	
}
