package com.lottery.controller;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.redirectedUrl;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.view;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.xpath;

import java.util.Locale;

import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentMatchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;

import com.lottery.model.Lottery;
import com.lottery.service.LotteryService;

@RunWith(SpringRunner.class)
@WebMvcTest(LotteryController.class) //which controller to be tested
public class LotteryControllerTest {

	private static final String PATH_LOTTERY_FORM = "/lottery-form.html";
	private static final String PATH_RESULT_PAGE = "/result";
	private static final String PATH_HOME_PAGE = "/";
	
	private static final String FORM_FIELD_FIRST_NUMBER = "firstNumber";
	private static final String FORM_FIELD_SECOND_NUMBER = "secondNumber";
	private static final String FORM_FIELD_THIRD_NUMBER = "thirdNumber";
	private static final String FORM_FIELD_FOURTH_NUMBER = "fourthNumber";
	private static final String FORM_FIELD_FIFTH_NUMBER = "fifthNumber";
	
	private static final String VIEW_LOTTERY_FORM = "lottery-form";
	
	private static final String CONTENTTYPE_HTML_UTF8 = "text/html;charset=UTF-8";
	
	@Autowired
	private MockMvc mvc;
	
	private ResultActions result;
	
	@MockBean
	private LotteryService lotteryService;
	
	private void given_theUserIsOnTheLotteryPage() throws Exception {
		result = mvc.perform(
				//post(PATH_LOTTERY_FORM)
				get(PATH_LOTTERY_FORM)
				.accept(MediaType.TEXT_HTML)
				.locale(Locale.ENGLISH))
				
				//.andDo(print()) //just for debug
				.andExpect(content().contentType(CONTENTTYPE_HTML_UTF8))
				.andExpect(status().isOk())
				.andExpect(view().name(VIEW_LOTTERY_FORM)
				);
	}
	
	private void when_theUserSubmitsTheLotteryFormContaining(final Lottery lotteryFormInput) throws Exception {
		result = mvc.perform(
				post(PATH_LOTTERY_FORM)
				.accept(MediaType.TEXT_HTML)
				.contentType(MediaType.APPLICATION_FORM_URLENCODED)
				.param(FORM_FIELD_FIRST_NUMBER, Integer.toString(lotteryFormInput.getFirstNumber()))
				.param(FORM_FIELD_SECOND_NUMBER, Integer.toString(lotteryFormInput.getSecondNumber()))
				.param(FORM_FIELD_THIRD_NUMBER, Integer.toString(lotteryFormInput.getThirdNumber()))
				.param(FORM_FIELD_FOURTH_NUMBER, Integer.toString(lotteryFormInput.getFourthNumber()))
				.param(FORM_FIELD_FIFTH_NUMBER, Integer.toString(lotteryFormInput.getFifthNumber()))
				);
	}
	
	private void when_userClicksOnTheHomeButton() throws Exception {
		result = mvc.perform(
				post(PATH_HOME_PAGE)
				.accept(MediaType.TEXT_HTML)
				);
	}
	
	private void then_theFormContains(final Lottery lottery) throws Exception {
		result.andExpect(xpath("//input[@name='" + FORM_FIELD_FIRST_NUMBER +"']/@value").string(Integer.toString(lottery.getFirstNumber())))
			  .andExpect(xpath("//input[@name='" + FORM_FIELD_SECOND_NUMBER +"']/@value").string(Integer.toString(lottery.getSecondNumber())))
			  .andExpect(xpath("//input[@name='" + FORM_FIELD_THIRD_NUMBER +"']/@value").string(Integer.toString(lottery.getThirdNumber())))
			  .andExpect(xpath("//input[@name='" + FORM_FIELD_FOURTH_NUMBER +"']/@value").string(Integer.toString(lottery.getFourthNumber())))
			  .andExpect(xpath("//input[@name='" + FORM_FIELD_FIFTH_NUMBER +"']/@value").string(Integer.toString(lottery.getFifthNumber())));
	}
	
	private void then_theUserIsRedirectedToTheResultPage() throws Exception {
		result.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl(PATH_RESULT_PAGE));
	}
	
	private void then_theUserIsRedirectedToTheHomePage() throws Exception {
		result.andExpect(status().is3xxRedirection())
			.andExpect(redirectedUrl(PATH_HOME_PAGE));
	}
	
	private void then_theQueryingIsRefusedDueToBadRequest() throws Exception {
		result.andExpect(status().isBadRequest())
			.andExpect(xpath("//div[@class='formErrorMessage']").exists());
	}
	
	private void then_theLotteryNumbersAreSentToTheService(final Lottery lottery) {
		verify(lotteryService, times(1)).checkIfTheUserHaveWon(lottery);
	}
	
	private void then_theLotteryNumbersAreNotSentToTheService() {
		verify(lotteryService, times(0)).checkIfTheUserHaveWon(ArgumentMatchers.any());
	}
	
	// TESTS
	
	//Scenario #1: The fields have zeros in them
	@Test
	@Ignore
	public  void testLotteryFormHasZerosWhenLotteryFormIsOpened() throws Exception {
		given_theUserIsOnTheLotteryPage();
		then_theFormContains(new Lottery(0, 0, 0, 0, 0));
		then_theLotteryNumbersAreNotSentToTheService();
	}
	
	//Scenario #2: The current lottery prize is showing
	@Test
	@Ignore
	public void testTheCurrentLotteryPrizeIsShowing() throws Exception {
		
	}
	
	//Scenario #3: Filling all the fields
	@Test
	@Ignore
	public void testFillingAllTheFields() throws Exception {
		
	}
	
	//Scenario #4: Leaving at least one field blank
	@Test
	@Ignore
	public void testLeavingAtLeastOneFieldBlank() throws Exception {
		
	}
	
	//Scenario #5: Leaving all fields blank
	@Test
	@Ignore
	public void testLeavingAllFieldsBlank() throws Exception {
		
	}
	
	//Scenario #6: Pressing the Home button
	@Test
	@Ignore
	public void testPressingTheHomeButton() throws Exception {
		given_theUserIsOnTheLotteryPage();
		when_userClicksOnTheHomeButton();
		then_theUserIsRedirectedToTheHomePage();
	}
	
	//Scenario #7: The entered numbers cannot be the same
	@Test
	@Ignore
	public void testTheEnteredNumbersCannotBeTheSame() throws Exception {
		
	}
	
}
