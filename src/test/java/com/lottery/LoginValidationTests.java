package com.lottery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lottery.model.Login;
import com.lottery.service.LoginService;
import com.lottery.service.exception.UserDoesNotExistException;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LoginValidationTests {

	@Autowired
	private LoginService loginService;
	
	@Test(expected = NullPointerException.class)
	public void test_validatingUserWithInvalidParameterNull_fails() throws UserDoesNotExistException {
		loginService.validateUser(null);
	}
	
	@Test(expected = UserDoesNotExistException.class)
	public void test_validatingUserWithInvalidParameter_fails() throws UserDoesNotExistException {
		loginService.validateUser(new Login("andrew", "asd"));
	}
	
}
