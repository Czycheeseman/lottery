package com.lottery;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lottery.model.Lottery;
import com.lottery.model.User;
import com.lottery.service.LotteryService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class LotteryValidationTests {

	@Autowired
	private LotteryService service;
	
	// getLotteryNumbersByGivenRowAmount()
	
	@Test
	public void test_gettingLotteryNumbersFromCSVFileWithValidParamter_successful() {
		service.getLotteryNumbersByGivenRowAmount(2);
	}
	
	@Test
	public void test_gettingLotteryNumbersFromCSVFileWithInvalidParamterZero_fails() {
		service.getLotteryNumbersByGivenRowAmount(0);
	}
	
	// getLotteryNumbersByGivenRowAmountExceptLastWeek()
	
	@Test
	public void test_gettingLotteryNumbersFromCSVFileExceptLastWeekWithValidParamter_successful() {
		service.getLotteryNumbersByGivenRowAmountExceptLastWeek(2);
	}
	
	@Test
	public void test_gettingLotteryNumbersFromCSVFileExceptLastWeekWithInvalidParamterZero_fails() {
		service.getLotteryNumbersByGivenRowAmountExceptLastWeek(0);
	}
	
	// checkIfTheUserHaveWon()
	
	@Test
	public void test_checkingIfTheUserHaveWonWithValidLotteryObject_successful() {
		service.checkIfTheUserHaveWon(new Lottery(1, 2, 3, 4, 5));
	}
	
	@Test
	public void test_checkingIfTheUserHaveWonWithInvalidLotteryObjectZero_fails() {
		service.checkIfTheUserHaveWon(new Lottery(0, 2, 4, 5, 6));
	}
	
	@Test
	public void test_checkingIfTheUserHaveWonWithInvalidLotteryObjectNinetyOne_fails() {
		service.checkIfTheUserHaveWon(new Lottery(1, 91, 4, 5, 6));
	}
	
	@Test(expected = NullPointerException.class)
	public void test_checkingIfTheUserHaveWonWithInvalidLotteryObjectNull_fails() {
		service.checkIfTheUserHaveWon(null);
	}
	
	@Test
	public void test_savingLotteryNumbersWithValidParameters_successful() {
		service.saveLotteryNumbers(new User("Andrew Taylor", "andrew@mail.com", "pa$$word"),  new Lottery(1, 2, 3, 4, 5));
	}
	
	@Test
	public void test_savingLotteryNumbersWithInvalidParameterUser_fails() {
		service.saveLotteryNumbers(new User("Andy", "andrew@mail.com", "pa$$word"),  new Lottery(1, 2, 3, 4, 5));
	}
	
	@Test
	public void test_savingLotteryNumbersWithInvalidParameterLottery_fails() {
		service.saveLotteryNumbers(new User("Andrew Taylor", "andrew@mail.com", "pa$$word"),  new Lottery(0, 2, 3, 4, 5));
	}
	
	@Test
	public void test_savingLotteryNumbersWithInvalidParameters_fails() {
		service.saveLotteryNumbers(new User("Andy", "mail.com", "pa$$"),  new Lottery(0, 2, 3, 88, 91));
	}
	
}
